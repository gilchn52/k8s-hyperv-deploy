#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive

# first of all we update the os to avoid unneccersry errors

sudo apt-get update -y 2>&1

# Kubernetes releases before v1.24 included a direct integration with Docker Engine, using a component named dockershim. That special direct integration is no 
# longer part of Kubernetes as of such , the below commands are needed . 
# Forwarding IPv4 and letting iptables see bridged traffic

sudo modprobe br_netfilter

# sysctl params required by setup, params persist across reboots

sudo chown vagrant:vagrant /etc/sysctl.conf
sudo chmod 777 /etc/sysctl.conf
sudo echo 'net.bridge.bridge-nf-call-iptables=1' >> /etc/sysctl.conf
sudo echo 'net.ipv4.ip_forward=1' >> /etc/sysctl.conf

# # Apply sysctl params without reboot
sudo sysctl --system

# Installing Needed Packages


sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release 2>&1


# add docker official gpg key
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg 2>&1


# Adding Docker Official Repos 

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update -y 2>&1


## Installing Docker 
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y 2>&1 

sudo chmod 666 /var/run/docker.sock
sudo systemctl enable docker.service 2>&1
sudo systemctl restart docker 2>&1


# Adding Current User to Docker Group Permissions 

sudo usermod -aG docker vagrant

# Disabling Swap - " You MUST disable swap in order for the kubelet to work properly."
sudo swapoff -a
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

# Adding K8S Repos and Installing K8S Binaries  

sudo curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add - 2>&1

echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list 2>&1

sudo apt-get update -y 2>&1

sudo apt-get install -y kubelet kubeadm kubectl 2>&1


# Setting Master Node Ip Address 

sudo touch /etc/default/kubelet
IP_ADDR=$(ip -br addr sh|grep UP|awk '{print $3}'|tail -n 1)
echo $IP_ADDR > myip
sudo chown vagrant:vagrant /etc/default/kubelet
sudo chmod 777 /etc/default/kubelet
sudo echo KUBELET_EXTRA_ARGS=--node-ip=192.168.50.11 > /etc/default/kubelet

sudo systemctl restart kubelet 

# this command allows us to avoid error while initiazliing the k8s master node
sudo rm /etc/containerd/config.toml

sudo systemctl restart containerd


mkdir -p /home/vagrant/.kube
sudo chmod 777 /home/vagrant/.kube
# we run this command as sh / bash in order to run in the highest privligbes and the localhost
sudo chmod 777 /vagrant/join.sh
sudo bash -c /vagrant/join.sh -v
sleep 60


	