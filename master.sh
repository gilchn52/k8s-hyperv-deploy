#!/bin/bash

# First of all we update the os.

sudo apt-get update -y 2> /dev/null

# to avoid this error message "dpkg-reconfigure: unable to re-open stdin: No file or directory" while installing updates
# "http://archive.ubuntu.com/ubuntu bionic-updates/universe amd64 apt-transport-https all 1.6.14"
# This makes debconf use a frontend that expects no interactive input at all, preventing it from even trying to access stdi






# Kubernetes releases before v1.24 included a direct integration with Docker Engine, using a component named dockershim. That special direct integration is no longer 
# part of Kubernetes as of such the below commands are needed:
# Forwarding IPv4 and letting iptables see bridged traffic
sudo modprobe br_netfilter

# sysctl params required by setup, params persist across reboots

sudo chown vagrant:vagrant /etc/sysctl.conf
sudo chmod 777 /etc/sysctl.conf
sudo echo 'net.bridge.bridge-nf-call-iptables=1' >> /etc/sysctl.conf
sudo echo 'net.ipv4.ip_forward=1' >> /etc/sysctl.conf

# # Apply sysctl params without reboot
sudo sysctl --system

# Installing Needed Packages and repos 
echo "installing needed packages and repos ......................................"
sleep 3 
echo "============================================================================"


sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release 2> /dev/null


# add docker official gpg key
echo "addding docker offifical gpg key .............................................."
sleep 3 
echo "============================================================================"

sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg 2>&1


# Adding Docker Official Repos 

echo "adding docker official repos ......................................................"
sleep 3
echo "====================================================================================="

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

echo "updateing os now ........................................................................"
sleep 3
echo "==========================================================================================="

sudo apt-get update -y 2> /dev/null


## Installing Docker 

echo "installing docker.............................................."
echo "=============================================================================="
sleep 3
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y 2>&1

sudo chmod 666 /var/run/docker.sock

# sudo systemctl status docker.service
sudo systemctl enable docker.service 2>&1
sudo systemctl restart docker


# Adding Current User to Docker Group Permissions 

sudo usermod -aG docker vagrant

# Disabling Swap - " You MUST disable swap in order for the kubelet to work properly."
sudo swapoff -a
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

# Adding K8S Repos and Installing K8S Binaries  

sudo curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add - 2>&1

echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list


sudo apt-get update -y 2> /dev/null

sudo apt-get install -y kubelet kubeadm kubectl 2> /dev/null

# adding mark - The kubelet is now restarting every few seconds, as it waits in a crashloop for kubeadm to tell it what to do.
sudo apt-mark hold kubelet kubeadm kubectl
# Setting Master Node Ip Address 

sudo touch /etc/default/kubelet


IP_ADDR=$(ip -br addr sh|grep UP|awk '{print $3}'|tail -n 1)
echo $IP_ADDR > myip
cp myip /vagrant/.vagrant/myip.sh
sudo chown vagrant:vagrant /etc/default/kubelet
sudo chmod 777 /etc/default/kubelet
sudo echo KUBELET_EXTRA_ARGS=--node-ip=192.168.50.10 > /etc/default/kubelet

sudo systemctl restart kubelet 

# this command allows us to avoid error while initiazliing the k8s master node
sudo rm /etc/containerd/config.toml

sudo systemctl restart containerd


# Initialize K8S

echo "initiliazing k8s ...." 
sleep 5 


sudo kubeadm init --ignore-preflight-errors CRI --apiserver-advertise-address="192.168.50.10" --apiserver-cert-extra-sans="192.168.50.10"  --node-name k8s-master --pod-network-cidr=10.244.0.0/16


echo "creating folders needed ...."
sleep 2 

echo "creating home/vagrant/.kube folder..."
sleep 3

mkdir -p /home/vagrant/.kube
chmod 777 /home/vagrant/.kube

if [ $? -eq 0 ]; then 
  sleep 2 && echo "Folder /home/vagrant/.kube created . command completed sucessfully";
else
  echo "Folder /home/vagrant/.kube wasn't created ! command failed";
fi

sudo cp -i /etc/kubernetes/admin.conf /home/vagrant/.kube/config
if [ $? -eq 0 ]; then 
  sleep 2 && echo "copying the admin.conf to /home/vagrant/.kube/config completed. command completed sucessfully";
else
  echo "command failed";
fi

sudo chown vagrant:vagrant /home/vagrant/.kube/config
sudo chmod 777 /home/vagrant/.kube/config
if [ $? -eq 0 ]; then 
  sleep 2 && echo "owenership of file /home/vagrant/.kube/config completed command completed sucessfully";
else
  echo "command failed";
fi



# install tigera calico plugin

echo "downloading calico yaml file"
sleep 3 


sudo curl https://docs.projectcalico.org/manifests/calico-typha.yaml -o calico.yaml 2>&1

if [ $? -eq 0 ]; then 
  sleep 2 && echo "calico yaml file downloaded sucesssfully";
else
  echo "calico file download failed" && exit 1;
fi

sudo chmod 777 calico.yaml 
sudo chown vagrant:vagrant calico.yaml

# we run this command because in vagrant all the script run in root privliege so the command will fail even if we run without sudo

runuser -l vagrant -c 'kubectl apply -f calico.yaml'

if [ $? -eq 0 ]; then 
  sleep 2 && echo "calico yaml apply sucesssfully ";
else
  echo "calico yaml apply failed" && exit 1;
fi


sudo curl -o calicoctl -O -L "https://github.com/projectcalico/calicoctl/releases/download/v3.19.1/calicoctl" 2>&1

if [ $? -eq 0 ]; then 
  sleep 2 && echo "calicoctl download successfully ";
else
  echo "calicoctl download failed";
fi
chmod 777 calicoctl
sudo mv calicoctl /usr/local/bin/

if [ $? -eq 0 ]; then 
  sleep 2 && echo "moving file calicoctl to /usr/local/bin applied successfully ";
else
  echo "moving file calicoctl to /usr/local/bin applied failed" && exit 1;
fi


sudo chmod +x /usr/local/bin/calicoctl

if [ $? -eq 0 ]; then 
  sleep 2 && echo "applying execute permisson to calicoctl applied sucessfully. ";
else
  echo "applying execute permisson to calicoctl applied failed" && exit 1;
fi




# Wait for the K8S Master node To Initalize
sleep 180
# Create the K8S Token and Then export it to bash script into localhost folder in order to use it in the slave node.
sudo kubeadm token create --print-join-command > /vagrant/.vagrant/join.sh
